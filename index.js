//3.

fetch('https://jsonplaceholder.typicode.com/todos/')
			.then((response) => response.json())
			.then((data) => console.log(data))
//4.
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((data) => {
	console.log(data.map(toDo => {
		return toDo.title
	}));
});



//5.
fetch('https://jsonplaceholder.typicode.com/todos/3')
			.then((response) => response.json())
			.then((data) => console.log(data))
//6.
fetch('https://jsonplaceholder.typicode.com/todos?id=3')
			.then((response) => response.json())
			.then((data) => {
				(data.map(toDo => {
					console.log('The item ' + '"' + toDo.title + '" ' + 'on the list has a status of ' + toDo.completed)


	}));
});

//7.

fetch('https://jsonplaceholder.typicode.com/todos',{
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		userID: 2,
		title: 'New To Do',
		body: 'New Body',
		completed: true
	})
})
.then((response) => (response.json()))
.then((data) => console.log(data))

// 8.
fetch('https://jsonplaceholder.typicode.com/todos/4',{
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		
		title: 'Updated To Do',
		body: 'Updated Body',
		
	})
})
.then((response) => (response.json()))
.then((data) => console.log(data))

//9.
fetch('https://jsonplaceholder.typicode.com/todos/6',{
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		
		title: 'change To Do',
		description: 'change description',
		status: false,
		dateCompleted: '20220629T000000-0400',
		userId: 4
		
	})
})
.then((response) => (response.json()))
.then((data) => console.log(data))

//10.

fetch('https://jsonplaceholder.typicode.com/todos/6',{
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		
		title: 'PATCH To Do',
		description: 'PATCH description',
		status: true,
		dateCompleted: '20220629T000000-0400',
		userId: 4
		
	})
})
.then((response) => (response.json()))
.then((data) => console.log(data))

//11.

fetch('https://jsonplaceholder.typicode.com/todos/9',{
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		completed: true,
		dateCompleted: '20220629T000000-0400',
		
	})
})
.then((response) => (response.json()))
.then((data) => console.log(data))

// 12.
fetch('https://jsonplaceholder.typicode.com/posts/21', {
	method: 'DELETE'
})
.then((response) => response.json())
.then((data) => console.log(data))